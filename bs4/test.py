#!/usr/bin/env python
#-*- coding: utf-8 -*-

import urllib
from bs4 import BeautifulSoup
from urlparse import urlparse

target_url = 'http://h21.hani.co.kr/arti/COLUMN/13/'
pages = 3

if __name__ == '__main__':
    print u"parsing : %s" % target_url
    turl = urlparse(target_url)

    base_host = "%s://%s" % (turl.scheme, turl.netloc)
    print u"base host :", base_host

    for page in range(1, (pages+1)):
        page_url = "%s?cline=%d" % (target_url, page)
        print u"PAGE %d - %s" % (page, page_url)
        list_doc = urllib.urlopen(page_url).read()
        soup = BeautifulSoup(list_doc, 'html.parser')

        articles = soup.select('article.item_article')
        print u"articles : %d" % len(articles)

        if len(articles) > 0:
            for arti in articles:
                print u'group:', arti.select('div.group1 a')[0].string
                print u'title:', arti.select('div.title1 a')[0].string, arti.select('div.title1 a')[0]['href']
                # href 로 같은지 판단 있으면 update 없으면 create
                print u'volume:', arti.select('div.magazine_no1 a')[0].string
                print u'prologue:', arti.select('div.prologue1 a')[0].string

                arti_url = arti.select('div.title1 a')[0]['href']
                arti_doc = urllib.urlopen("%s%s" % (base_host, arti_url)).read()
                insoup = BeautifulSoup(arti_doc, 'html.parser')
                print u'group2:', insoup.select('header.article_head h5 a')[0].string, insoup.select('header.article_head h5 a')[0]['href']
                print u'image2:', insoup.select('meta[property=og:image]')[0]['content']
                print u'dates:', insoup.select('div.datebox')[0].string

                print '---------------------------'
        else:
            break;
        #print arti.prettify()
