#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import getopt
import re
import csv
import glob
import time
import sqlite3
import shutil
import json

from datetime import datetime, timedelta
from MySQLDrive import *

def print_usage():
    print u"USAGE : %s OPTIONS" % os.path.basename(__file__)
    print u"  --app : app domain id. ex) --app=kr.co.kyobobook.KyoboComix"
    print u"  --catalog : target catalog sqlite file path. ex) --catalog=Default"
    print u"  --showcase : target catalog sqlite file path. ex) --showcase=best25h"
    print u"  -p, --period : query period. ex) -p 24h, --period=7d"
    print u"  -l, --limit : set rows limit. ex) -l 1000"
    sys.exit(1)

#..............................................................................

class Options:

    def __init__(self, args, production=False):
        try:
            opts, remains = getopt.getopt(args, "p:l:", ["period=", "limit=", "app=", "catalog=", "showcase="])
        except getopt.GetoptError as err:
            print str(err)
            sys.exit(1)
        
        if production:
            self.apps_path = "/var/storage/apps" 
        else:
            self.apps_path = "/home/bookjam/storage/apps"
        
        self.limit = -1
        
        for key, value in opts:
            if key == "--app":
                self.app_id = value
            elif key == "--period" or key == "-p":
                self.period = value
            elif key == "--limit" or key == "-l":
                self.limit = value
            elif key == "--catalog":
                self.catalog = value
            elif key == "--showcase":
                self.showcase = value
                
class StoreMysql:
    
    def __init__(self, opts):
        self.app_id = opts.app_id
        self.period = opts.period
        self.limit = opts.limit
        
        self.db = MySQLDrive('localhost', 'bookjam', 'qnrwoa2012', 'bookjam_store')
        self.app = self.get_app(self.app_id)
        
    def get_app(self, app_id):
        self.db.select(u"SELECT * FROM data_apps WHERE app_domain = '%s'" % self.app_id)
        return self.db.one("dict")

    def get_rank_by_receipt(self):
        sp, ep = self.get_timestamp_from_period(self.period)
    
        query = u"SELECT AA.series_id, BB.title, COUNT(*) AS amount \
                    FROM \
                        (SELECT A.*, B.title, CONVERT(JSON_EXTRACT(B.meta, 'series') USING utf8) AS series_id \
                            FROM  \
                                (SELECT otid, product_id, device_id FROM log_receipt \
                                    WHERE app_id = %d AND store = 'bookjam' AND created_at BETWEEN %d AND %d \
                                    GROUP BY otid, product_id, device_id) A, \
                                data_products B \
                            WHERE B.type = 'product' AND A.product_id = B.id AND B.title NOT LIKE '%%코인') AA, \
                        data_products BB \
                    WHERE BB.type = 'series' AND AA.series_id = BB.id \
                    GROUP BY AA.series_id \
                    ORDER BY amount DESC" % (self.app["id"], sp, ep)       

        if self.limit > 0: query = query + " LIMIT 0, %d" % self.limit
        
        self.db.select(query)
        return self.db.all("dict")
                    
    def get_timestamp_from_period(self, period):
        pattern = r'(\d+)([ymdh]?)$'
        p = re.compile(pattern)
        units = p.search(period)
        times = int(units.group(1));
        
        end_stamp = int(time.time())
        AN_HOUR = 3600 #secs
        
        if units.group(2) == 'y':
            begin_stamp = end_stamp - (AN_HOUR * 24 * 365 * times)
        elif units.group(2) == 'm':
            begin_stamp = end_stamp - (AN_HOUR * 24 * 30 * times)
        elif units.group(2) == 'd':
            begin_stamp = end_stamp - (AN_HOUR * 24 * times)
        elif units.group(2) == 'h':
            begin_stamp = end_stamp - (AN_HOUR * times)

        return begin_stamp, end_stamp
                    
class CatalogSqlite:

    def __init__(self, opts):
        self.db_file = "%s/%s/Catalogs/%s.sqlite" % (opts.apps_path, opts.app_id, opts.catalog)
        self.showcase = opts.showcase
        
        if not os.path.exists("%s.base" % self.db_file):
            print "CatalogSqlite Error: base sqlite file not found. %s" % self.db_file
            sys.exit(1)
        
        try:
            self.connection = sqlite3.connect("%s.base" % self.db_file)
            self.cursor = self.connection.cursor()
        except sqlite3.Error as err:
            print str(err)
            self.cursor.close()
            self.connection.close()
            sys.exit(1)
            
    def __del__(self):
        self.cursor.close()
        self.connection.close()
    
    def flush(self):
        self.cursor.execute(u"DELETE FROM showcases WHERE showcase = '%s'" % self.showcase.lower())
        self.connection.commit()
    
    def find_auxiliary(self, series_id):
        self.cursor.execute(u"SELECT id FROM showcases WHERE showcase = 'series' AND series = '%s'" % series_id)
        return self.cursor.fetchone()
        
    def fill_showcases(self, rows):
        rank = 1
        try:
            for row in rows:
                aux = self.find_auxiliary(row["series_id"])
                if aux is None: continue
                
                print u"('S_%s_%06d', '%s', '%s', '%06d', '%s', %d)" % (self.showcase.upper(), rank, self.showcase.lower(), row["series_id"], rank, row["title"], row["amount"])
                
                attr = { "auxiliary": aux[0], \
                         "id": "S_%s_%06d" % (self.showcase.upper(), rank), \
                         "rank":"%06d" % rank, \
                         "series": row["series_id"] }
                attr = json.dumps(attr)
                
                #self.cursor.execute(u"INSERT INTO showcases \
                #    (id, showcase, attr) VALUES \
                #    ('S_%s_%06d', '%s', '%s')" \
                #    % (self.showcase.upper(), rank, self.showcase.lower(), attr))
                    
                rank += 1
                
        except sqlite3.Error as err:
            print str(err)
            self.cursor.close()
            self.connection.close()
            sys.exit(1)
            
        self.connection.commit()

    def file_backup(self):
        self.backup_file = "%s.bak" % self.db_file
        shutil.copy(self.db_file, self.backup_file)
        
    def file_rollback(self):
        self.backup_file = "%s.bak" % self.db_file
        shutil.copy(self.backup_file, self.db_file)
        
    def file_update(self):
        self.base_file = "%s.base" % self.db_file
        shutil.copy(self.base_file, self.db_file)
    
#..............................................................................
        
if __name__ == '__main__':
    options = Options(sys.argv[1:], True)
    
    store = StoreMysql(options)
    rows = store.get_rank_by_receipt()
    
    cat = CatalogSqlite(options)
    cat.file_backup()
    #cat.flush()
    cat.fill_showcases(rows)
    #cat.file_update()

