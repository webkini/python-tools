#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import MySQLdb as mysql

# utf-8 support

reload(sys)
sys.setdefaultencoding("utf-8")

# MysqlDrive

class MySQLDrive:
    
    def __init__(self, host, user, password, dbname):
        self.dbname = dbname
        self.query  = []
        try:
            self.dbconn = mysql.connect(host=host, user=user, passwd=password, db=dbname, \
                                        charset='utf8', init_command='SET NAMES UTF8')
            self.cursor = self.dbconn.cursor()
        except mysql.Error, e:
            print u"connect : error %d, %s" % (e.args[0], e.args[1])
            exit(1)
            
    def __del__(self):
        self.cursor.close()
        self.dbconn.close()
        
    def select(self, query):
        try:
            self.cursor.execute(query)
        except mysql.Error, e:
            print u"select : error %d, %s" % (e.args[0], e.args[1])
            
    def statement(self, query):
        try:
            self.cursor.execute(query)
            self.dbconn.commit() #do it only innodb
        except mysql.Error, e:
            print u"statement : error %d, %s" % (e.args[0], e.args[1])
            self.dbconn.rollback()
        
    def all(self, type="list"):
        try:
            if type == "dict":
                rows = []
                for row in self.cursor.fetchall():
                    rows.append(self._convert_a_dict(row))
                return rows
            else:        
                return self.cursor.fetchall()
        except mysql.Error, e:
            print u"all : error %d, %s" % (e.args[0], e.args[1])
            exit(1)
        
    def one(self, type="list"):
        try:
            if type == "dict":
                return self._convert_a_dict(self.cursor.fetchone())
            else:
                return self.cursor.fetchone()
        except mysql.Error, e:
            print u"one : error %d, %s" % (e.args[0], e.args[1])
            exit(1)
    
    def _convert_a_dict(self, row, table=""):
        if table == "":
            column_names = tuple([col[0] for col in self.cursor.description])
        else:
            self.select(u"SHOW columns FROM %s" % str(table))
            column_names = tuple([col[0] for col in self.cursor.fetchall])
        return dict(zip(column_names, row))
        
    def affected_rows(self):
        return self.cursor.rowcount()
        
    def insert_dict(self, table, dict): #dictionary type
        keys = [u"`%s`" % self.dbconn.escape_string(str(f)) for f in dict.keys()]
        values = [u"'%s'" % self.dbconn.escape_string(str(v)) for v in dict.values()]
        query = u"INSERT INTO %s (%s) VALUES (%s)" % ( table, ','.join(tuple(keys)), ','.join(tuple(values)) )
        self.statement(query)
    
    def insert_list(self, table, vals): #list type(only values)
        values = [u"'%s'" % self.dbconn.escape_string(str(v)) for v in vals]
        query = u"INSERT INTO %s VALUES (%s)" % ( table, ','.join(tuple(values)) )
        self.statement(query)
    
    def update(self, table, dict, where="1"):
        keys = dict.keys()
        set_values = [u"`%s`='%s'" % (k, self.dbconn.escape_string(str(dict[k]))) for k in keys]
        query = u"UPDATE %s SET %s WHERE %s" % ( table, ','.join(tuple(set_values)), where )
        self.statement(query)

        